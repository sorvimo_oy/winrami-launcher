# WinRami Launcher

WinRami is SSAB's 2.5 D frame and truss design software. **Please note that this
software is completely separate from WinRami and is not endorsed or
developed by SSAB.**

This is a helper tool that attempts to fix an UI bug in WinRami. In some systems
WinRami's coordinate toolbar is only shown partially:

![Broken coordinate toolbar](docs/broken.png)

At the moment this tool provides a crude way to force the coordinate toolbar to
become fully extended:

![Fixed coordinate toolbar](docs/fixed.png)

## How it works

This tool tries to detect the coordinate toolbar's window using the Win32 API If
a suitable window is detected the tool starts to poll the window's size for
change. Once the toolbar reaches its final height a check is made to see if the
height below a set value. If needed the height of the toolbar is adjusted to the
set value.

As a naive polling solution is used there is small delay between the the toolbar
reaching its final height and the height being adjusted.

## How to use

A precompiled binary can be downloaded from
[here](https://old-ruukki-software.s3.eu-north-1.amazonaws.com/WinRami-Launcher.exe)
or you can build the tool yourself by following the steps in the next section.

To run WinRami using this fix, first start WinRami-Launcher and set the path to
WinRami's location (the default path should work if WinRami is installed using
the default configuration) and the required toolbar height (30 seems to work
well). Clicking the "Start WinRami" button saves the settings, starts WinRami,
and attempts to hook into the toolbar window.

## Building the tool as an EXE

First clone this repository using `git` and install `python3.9` using your
prefered channel.

Install Pipenv:

```
python -m pip install --user pipenv
```

Navigate to the cloned folder and setup new virtual environment:

```
pipenv install
```

Activate the virtual environment and run the build script:

```
pipenv shell
.\build.ps1
```

After this the WinRami-Launcher.exe can be found from `/dist` folder.

## License

This software is distributed under MIT license. For more information see LICENSE
in this directory.
