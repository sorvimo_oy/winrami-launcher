from json import dump, load
from os.path import abspath, join
from pathlib import Path
from threading import Thread
from subprocess import Popen
import sys
from tkinter import Button, Label, Text, Tk, Toplevel
from tkinter.constants import DISABLED, END, NORMAL
from tkinter.ttk import Frame
from typing import Optional

from popup_detection import detect_winrami_popup


DEFAULT_WINRAMI_PATH = "C:/Program Files (x86)/RUUKKI/WINRAMI/5.00/Winrami.exe"
DEFAULT_TOOLBAR_HEIGHT = 30

SETTINGS_FILE_NAME = ".winrami-launcher.json"
SETTINGS_FILE_PATH = join(str(Path.home()), SETTINGS_FILE_NAME) 


def resource_path(relative_path: str) -> str:
    try:
        base_path = sys._MEIPASS
    except:
        base_path = abspath(".")

    return join(base_path, relative_path)


class App(Frame):

    def __init__(self, master: Tk) -> None:
        super().__init__(master, padding=10)

        master.iconbitmap(resource_path("data\\favicon.ico"))
        master.title("WinRami Launcher")

        self._polling_thread: Optional[Thread] = None

        self._read_settings()
        self._init_elements()

    def run(self) -> None:
        self.mainloop()

        if self._polling_thread:
            self._polling_thread.join()

    def _init_elements(self) -> None:
        self.grid()

        Label(self, text="WinRami path").grid(column=0, row=0)
        self._winrami_path_text = Text(self, height=1, width=20)
        self._winrami_path_text.grid(column=1, row=0)

        self._winrami_path_text.insert(END, self._winrami_path)

        Label(self, text="Toolbar height").grid(column=0, row=1)
        self._toolbar_height_text = Text(self, height=1, width=5)
        self._toolbar_height_text.grid(column=1, row=1)

        self._toolbar_height_text.insert(END, self._toolbar_height)

        self._quit_button = Button(self, text="Quit", command=self._quit)
        self._quit_button.grid(column=0, row=2)

        self._start_winrami_button = Button(self, text="Start WinRami", command=self._start_winrami)
        self._start_winrami_button.grid(column=1, row=2)

    def _start_winrami(self) -> None:
        self._winrami_path = self._winrami_path_text.get("1.0", "end-1c")

        try:
            toolbar_height = int(self._toolbar_height_text.get("1.0", "end-1c"))
        except:
            toolbar_height = 30
            self._toolbar_height_text.delete("1.0", END)
            self._toolbar_height_text.insert(END, toolbar_height)

        self._toolbar_height = toolbar_height

        try:
            Popen(self._winrami_path)
        except:
            self._handle_failed_to_start_winrami()
        else:
            self._write_settings()

            self._quit_button["state"] = DISABLED
            self._start_winrami_button["state"] = DISABLED

            if self._polling_thread:
                self._polling_thread.join()

            self._polling_thread = Thread(
                target=detect_winrami_popup,
                kwargs={
                    "height_limit": self._toolbar_height,
                    "on_quit": self._handle_winrami_exit,
                }
            )
            self._polling_thread.start()

    def _handle_failed_to_start_winrami(self) -> None:
        win = Toplevel(padx=10, pady=10)
        win.iconbitmap(resource_path("data\\favicon.ico"))
        win.title("WinRami Launcher - Failure!")

        Label(win, text="WinRami executable was not found in specified path").grid(row=0, column=0)
        Button(win, text="Ok", command=win.destroy).grid(row=1, column=0)

    def _handle_winrami_exit(self) -> None:
        self._quit_button["state"] = NORMAL
        self._start_winrami_button["state"] = NORMAL

    def _quit(self) -> None:
        self.master.destroy()

    def _read_settings(self) -> None:
        try:
            with open(SETTINGS_FILE_PATH, "r") as file:
                settings_data = load(file)
        except:
            settings_data = {}
        finally:
            self._winrami_path = settings_data.get("winrami_path", DEFAULT_WINRAMI_PATH)
            self._toolbar_height = settings_data.get("toolbar_height", DEFAULT_TOOLBAR_HEIGHT)

    def _write_settings(self) -> None:
        settings_data = {
            "winrami_path": self._winrami_path,
            "toolbar_height": self._toolbar_height,
        }

        with open(SETTINGS_FILE_PATH, "w+") as file:
            dump(settings_data, file)
