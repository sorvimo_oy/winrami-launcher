from dataclasses import dataclass


@dataclass
class WindowOfIntrest:

    handle: int
    current_width: int
    current_height: int
    visible: bool
    count: int = 0
    change_detected: bool = False

    def update(self, new_width: int, new_height: int, new_visibility: bool):
        self.current_width = new_width

        old_height = self.current_height
        self.current_height = new_height

        old_visibility = self.visible
        self.visible = new_visibility

        if old_height != new_height or old_visibility != new_visibility:
            self.change_detected = True
