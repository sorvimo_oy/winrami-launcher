from time import sleep
from typing import Callable, List

from pywintypes import error
from win32gui import EnumChildWindows, GetWindowRect, EnumWindows, IsWindowVisible, MoveWindow

from window import WindowOfIntrest


def detect_winrami_popup(*, height_limit: int, on_quit: Callable) -> None:
    matching_windows: List[WindowOfIntrest] = []

    while len(matching_windows) == 0:
        EnumWindows(_enum_windows_callback, matching_windows)
        sleep(1.0)

    done = False

    while not done:
        change_detected_in_window = False

        for window in matching_windows:
            try: 
                (left, top, right, bottom) = GetWindowRect(window.handle)
            except error:
                done = True

            width = right - left
            height = bottom - top
            visible = IsWindowVisible(window.handle)

            window.update(width, height, visible)

            if window.change_detected:
                change_detected_in_window = True

            if visible and height < height_limit and window.change_detected:
                window.count += 1

                if window.count >= 2:
                    window.count = 0
                    window.change_detected = False
                    width = right - left
                    MoveWindow(window.handle, left, top, width, height_limit, True)

        sleep(0.5 if change_detected_in_window else 1.0)

    on_quit()


def _enum_windows_callback(hWindow: int, window_handles: List[WindowOfIntrest]) -> None:
    def enum_child_callback(hWindow: int, window_handles: List[int]) -> None:
        window_handles.append(hWindow)

    visible = IsWindowVisible(hWindow)
    (left, top, right, bottom) = GetWindowRect(hWindow)
    width = right - left
    height = bottom - top

    if not visible and width == 100 and height == 100:
        child_windows = []
        EnumChildWindows(hWindow, enum_child_callback, child_windows)

        if len(child_windows) == 15:
            window_handles.append(
                WindowOfIntrest(
                    handle=hWindow,
                    current_width=width,
                    current_height=height,
                    visible=visible,
                )
            )
