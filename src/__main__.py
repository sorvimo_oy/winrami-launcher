from tkinter import Tk

from app import App


def main() -> None:
    app = App(Tk())
    app.run()


if __name__ == "__main__":
    main()
