pyinstaller `
    --onefile `
    --hidden-import win32timezone `
    --paths .\src\ `
    --noconsole `
    --add-data="data/favicon.ico;data" `
    --name WinRami-Launcher `
    --icon "data/favicon.ico" `
    .\src\__main__.py
